<?php
// Collect all collections shown in order and add their sub-items
$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
    
$allCollections =  get_records('Collection', $params, 10000);

/*
foreach ($items as $item) {
    $allItems = array_merge($allItems, CsvExport_ItemAttachUtil::getThisAndAnnotations($item));
}
*/
// Render CSV
printCollectionsCsvExport($allCollections);
