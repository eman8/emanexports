# EMAN Exports

By Vincent BUARD for ENS-ITEM

This is a modified version of the CSV Export module [CSV Export plugin](http://omeka.org/classic/plugins/CsvExport/).

It offers the same functionality but :

- the produced files include all the result pages (and not just the currently displayed one).
- it offers an XML-TEI export format.

This last format is currently being designed. As of now it is strictly equivalent to the omeka-xml format.


## License

This plugin is licensed under Apache License 2.0.

## Credits

Plugin réalisé pour la plate-forme EMAN (ENS-CNRS-Sorbonne nouvelle) par Vincent Buard (Numerizen), avec le soutien du consortium Cahiers : http://cahier.hypotheses.org. Voir les explications sur le site [EMAN](https://eman-archives.org/EMAN/pluginseman#B4)
